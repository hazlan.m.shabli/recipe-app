RECIPE APP

a tutorial to demonstrate the use of graphql + nodejs + sequelize + sqlite + apollo-server + bcrypt + graphql.
This is just a simple workflow and will not cover the authentication part of the process. bcrypt is used to encrypt the password when creating a user but ideally when creating recipe the authentication would be required.

This is taken from https://scotch.io/tutorials/super-simple-graphql-with-node
the original tutorial had some syntax error and no clear instruction that user need to be created before adding recipe.

insructions:
1. git clone https://gitlab.com/hazlan.m.shabli/recipe-app
2. cd server (all instructions below need to be run in server folder)
3. run yarn
4. create database.sqlite
5. run node_modules/.bin/sequelize db:migrate
6. run node src/index.js

Note:
User need to be created in the database before the recipe can be created

in playground (localhost:4000)
to add User ->

mutation{
  createUser(name: "Your User Name", 
    email: "your email",password: "your password"){
    name
    email
    password
  }
}

to add Recipe ->

mutation {
  createRecipe(
    userId: 1
    title: "Sample 2"
    ingredients: "Salt, Pepper"
    direction: "Add salt, Add pepper"
  ) {
    id
    title
    ingredients
    direction
    user {
      id
      name
      email
    }
  }
}
you can verify the result by opening the database.sqlite in your databse viewer.


